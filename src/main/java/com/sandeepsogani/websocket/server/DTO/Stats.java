package com.sandeepsogani.websocket.server.DTO;

import java.util.List;

public class Stats {
    private Integer score;
    private Integer time;
    private long roundStartTimestamp;
    private long roundEndTimestamp;
    private List<Integer> events;

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public long getRoundStartTimestamp() {
        return roundStartTimestamp;
    }

    public void setRoundStartTimestamp(long roundStartTimestamp) {
        this.roundStartTimestamp = roundStartTimestamp;
    }

    public long getRoundEndTimestamp() {
        return roundEndTimestamp;
    }

    public void setRoundEndTimestamp(long roundEndTimestamp) {
        this.roundEndTimestamp = roundEndTimestamp;
    }

    public List<Integer> getEvents() {
        return events;
    }

    public void setEvents(List<Integer> events) {
        this.events = events;
    }

    public void appendEvent(int event){
        this.events.add(event);
    }

    public Boolean findContinuousTimeout(List<Integer> events){
        int size = events.size();
        return size >= 3 && (events.get(size - 1).equals(1) && events.get(size - 2).equals(1) && events.get(size - 3).equals(1));
    }
}
