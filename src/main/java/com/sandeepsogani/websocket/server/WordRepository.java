package com.sandeepsogani.websocket.server;

import com.sandeepsogani.websocket.server.DTO.Word;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class WordRepository {

    private final static Map<String, Word> WORDS = new HashMap<String, Word>() {
        private static final long serialVersionUID = 1L;
        {
            put("a", new Word("a", "a"));
            put("s", new Word("s", "s"));
            put("d", new Word("d", "d"));
            put("q", new Word("q", "q"));
            put("w", new Word("w", "w"));
            put("e", new Word("e", "e"));
            put("z", new Word("z", "z"));
            put("x", new Word("x", "x"));
            put("c", new Word("c", "c"));
            put("m", new Word("m", "m"));
            put("l", new Word("l", "l"));
            put("p", new Word("p", "p"));
            put("i", new Word("i", "i"));
            put("g", new Word("g", "g"));
        }
    };

    private static final WordRepository INSTANCE = new WordRepository();

    private List<String> keys = new ArrayList<String>(WORDS.keySet());

    private Random random = new Random();

    private WordRepository() {

    }

    public static WordRepository getInstance() {
        return INSTANCE;
    }

    public Word getRandomWord() {
        return WORDS.get(keys.get(random.nextInt(keys.size())));
    }

    public Word getWord(String key) {
        return WORDS.get(key);
    }

}