package com.sandeepsogani.websocket.server;

import com.sandeepsogani.websocket.server.DTO.Stats;

import javax.websocket.*;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

@ServerEndpoint(value = "/game")
public class SocketServerEndpoint {
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @OnOpen
    public void onOpen(Session session) {
        logger.info("Connected ... " + session.getId());
    }

    @OnMessage
    public String onMessage(String value, Session session) throws IOException {
        switch (value) {
            case "start":
                logger.info("Starting the game by sending first word");
                String word = WordRepository.getInstance().getRandomWord().getKey();
                session.getUserProperties().put("word", word);
                List<Integer> events = new ArrayList<>();
                long start = System.currentTimeMillis();
                Stats stats = new Stats();
                stats.setScore(0);
                stats.setTime(getTimeout());
                stats.setEvents(events);
                stats.setRoundStartTimestamp(start);
                session.getUserProperties().put("stats", stats);
                return String.format("Start Follow me \n(Score: %s, Timeout: %s sec, Timeout Events : %s ) \n Type:  %s",
                        stats.getScore(),stats.getTime(), stats.getEvents().toString(), word);
            case "quit":
                logger.info("Quitting the game");
                handleExit(session);
        }
        return handleAnswer(value, session);
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        logger.info(String.format("Session %s closed because of %s", session.getId(), closeReason));
    }

    private static int getTimeout() throws IOException{
        final Properties properties = new Properties();
        properties.load(SocketServerEndpoint.class.getClassLoader().getResourceAsStream("config.properties"));
        return Integer.parseInt(properties.getProperty("sessionTimeout","5"));
    }

    private void handleExit(Session session){
        Stats stats = (Stats) session.getUserProperties().get("stats");
        try {
            session.close(new CloseReason(CloseCodes.NORMAL_CLOSURE,
                    String.format("Game finished. (Score: %s, Timeout: %s sec, Timeout Events : %s )",
                            stats.getScore(),stats.getTime(), stats.getEvents().toString())));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String handleAnswer(String value, Session session){
        String word = (String) session.getUserProperties().get("word");
        Stats stats = (Stats) session.getUserProperties().get("stats");
        float sec = (System.currentTimeMillis() - stats.getRoundStartTimestamp()) / 1000F;
        System.out.println(sec + " seconds");
        String responseBuilder ="";
        if (sec > stats.getTime())  {
            stats.getEvents().add(1);
            responseBuilder+= "Time limit Exceeded. Please Respond in Time.";
        } else if (word != null && word.equals(value)) {
            stats.setScore(stats.getScore()+1);
            stats.getEvents().add(0);
            responseBuilder+= "You pressed it right.";
        } else {
            stats.setScore(stats.getScore()-1);
            stats.getEvents().add(0);
            responseBuilder+= "You pressed it wrong.";
        }
        return handleGameOver(stats,session,responseBuilder);
    }

    private String handleGameOver(Stats stats, Session session,String ResponseBuilder){

        if (stats.getScore()>=10 || stats.getScore()<=-3 || stats.findContinuousTimeout(stats.getEvents())){
            handleExit(session);
        }
        WordRepository repository = WordRepository.getInstance();
        String nextWord = repository.getRandomWord().getKey();
        session.getUserProperties().put("word", nextWord);
        stats.setRoundStartTimestamp(System.currentTimeMillis());
        return String.format(" %s \n(Score: %s, Timeout: %s sec, Timeout Events : %s ) \n Type the next word :  %s",
                ResponseBuilder,stats.getScore(),stats.getTime(), stats.getEvents().toString(), nextWord);
    }

}
