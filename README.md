# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Websocket Game
* Version:1.0

### How do I get set up? ###

* Clone the repository
* Pre-Requisite: JAVA-8, Maven

### Deployment instructions
* cd /working/directory
* Open Terminal1
* Run the Server: mvn compile exec:java -Dexec.mainClass="com.sandeepsogani.websocket.server.SocketServer"
* Open Terminal2
* Run the client: mvn compile exec:java -Dexec.mainClass="com.sandeepsogani.websocket.client.SocketClientEndpoint"
* Follow the instructions to complete the Game
* Sample Result-> Score: 2, Timeout: 5 sec, Timeout Events : [1, 0, 0]
* You Configure Timeout in src/main/resources/config.properties
* Timeout Events shows results from the begin of session appended to a list.
* 1- if session is timeout
* 0- if reponse was provided within time

